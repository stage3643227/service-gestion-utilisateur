package tn.sofrecom.servicegestionacces;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import tn.sofrecom.servicegestionacces.dto.roles.RoleRequestDTO;
import tn.sofrecom.servicegestionacces.dto.roles.RoleResponseDTO;
import tn.sofrecom.servicegestionacces.handlers.permissions.PermissionsServiceBusinessException;
import tn.sofrecom.servicegestionacces.handlers.roles.RolesServiceBusinessException;
import tn.sofrecom.servicegestionacces.mappers.RoleMapperImpl;
import tn.sofrecom.servicegestionacces.model.Permission;
import tn.sofrecom.servicegestionacces.model.Role;
import tn.sofrecom.servicegestionacces.repository.PermissionRepository;
import tn.sofrecom.servicegestionacces.repository.RoleRepository;
import tn.sofrecom.servicegestionacces.service.roles.implement.RolesServiceImp;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
class RoleServiceImpTest {

    @Mock
    private RoleRepository roleRepository;

    @Mock
    private RoleMapperImpl roleMapper;

    @Mock
    private PermissionRepository permissionRepository;

    @InjectMocks
    private RolesServiceImp rolesService;

    private RoleRequestDTO roleRequestDTO;
    private Role role;
    private Role savedRole;
    private RoleResponseDTO expectedRoleResponseDTO;
    private List<Role> rolesList;

    private List<RoleResponseDTO> rolesResponseList;
    private List<Permission> permissionsList;

    private Permission permissionRead;

    private Permission permissionWrite;

    private List<Long> permissionIds;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.openMocks(this);
        roleRepository = Mockito.mock(RoleRepository.class);
        roleMapper = Mockito.mock(RoleMapperImpl.class);
        permissionRepository = Mockito.mock(PermissionRepository.class);
        rolesService = new RolesServiceImp(roleRepository, permissionRepository, roleMapper);

        permissionsList = new ArrayList<>();
        permissionsList.add(new Permission(1L, "read", null));
        permissionsList.add(new Permission(2L, "write", null));

        permissionIds = new ArrayList<>();
        permissionIds.add(1L);
        permissionIds.add(2L);

        roleRequestDTO = new RoleRequestDTO();
        roleRequestDTO.setIdRole(1L);
        roleRequestDTO.setRoleName("Admin");
        roleRequestDTO.setPermissionIds(permissionIds);

        savedRole = new Role();
        savedRole.setIdRole(1L);
        savedRole.setRoleName("Admin");
        savedRole.setPermissions(permissionsList);

        role = new Role();
        role.setIdRole(1L);
        role.setRoleName("Admin");
        role.setPermissions(permissionsList);


        expectedRoleResponseDTO = new RoleResponseDTO();
        expectedRoleResponseDTO.setIdRole(1L);
        expectedRoleResponseDTO.setRoleName("Admin");
        expectedRoleResponseDTO.setPermissions(permissionsList);

        permissionRead = new Permission();
        permissionRead.setIdPermission(1L);
        permissionRead.setPermissionName("read");

        permissionWrite = new Permission();
        permissionWrite.setIdPermission(1L);
        permissionWrite.setPermissionName("write");


        rolesList = new ArrayList<>();
        rolesList.add(new Role(1L, "Admin", null, null, permissionsList));
        rolesList.add(new Role(2L, "simple user", null, null, permissionsList));


        rolesResponseList = new ArrayList<>();
        rolesResponseList.add(new RoleResponseDTO(1L, "Admin", permissionsList));
        rolesResponseList.add(new RoleResponseDTO(2L, "simple user", permissionsList));
    }

    @Test
    void testCreateNewRole() {
        when(roleMapper.roleDtoToRole(roleRequestDTO)).thenReturn(role);
        when(permissionRepository.findById(roleRequestDTO.getPermissionIds().get(0))).thenReturn(Optional.of(permissionRead));
        when(permissionRepository.findById(roleRequestDTO.getPermissionIds().get(1))).thenReturn(Optional.of(permissionWrite));
        when(roleRepository.saveAndFlush(role)).thenReturn(savedRole);
        when(roleMapper.roleToRoleResponseDto(savedRole)).thenReturn(expectedRoleResponseDTO);

        RoleResponseDTO actualRoleResponseDTO = rolesService.createNewRole(roleRequestDTO);
        assertNotNull(actualRoleResponseDTO);
        assertEquals(actualRoleResponseDTO.getIdRole(), expectedRoleResponseDTO.getIdRole());
        assertEquals(actualRoleResponseDTO.getRoleName(), expectedRoleResponseDTO.getRoleName());

    }

    @Test
    void testUpdateRole() {
        when(roleRepository.findById(roleRequestDTO.getIdRole())).thenReturn(Optional.of(role));
        when(roleMapper.roleDtoToRole(expectedRoleResponseDTO)).thenReturn(role);
        when(roleMapper.roleDtoToRole(roleRequestDTO)).thenReturn(role);
        when(permissionRepository.findById(roleRequestDTO.getPermissionIds().get(0))).thenReturn(Optional.of(permissionRead));
        when(permissionRepository.findById(roleRequestDTO.getPermissionIds().get(1))).thenReturn(Optional.of(permissionWrite));
        when(roleRepository.saveAndFlush(role)).thenReturn(savedRole);
        when(roleMapper.roleToRoleResponseDto(savedRole)).thenReturn(expectedRoleResponseDTO);

        RoleResponseDTO actualRoleResponseDTO = rolesService.updateRole(roleRequestDTO);
        assertNotNull(actualRoleResponseDTO);
        assertEquals(expectedRoleResponseDTO.getIdRole(), expectedRoleResponseDTO.getIdRole());
        assertEquals(expectedRoleResponseDTO.getRoleName(), expectedRoleResponseDTO.getRoleName());
        assertEquals(expectedRoleResponseDTO.getPermissions(), expectedRoleResponseDTO.getPermissions());
    }

    @Test
    void testDeleteRole() {
        Long idRole = 1L;
        when(roleRepository.findById(roleRequestDTO.getIdRole())).thenReturn(Optional.of(role));
        when(roleMapper.roleDtoToRole(expectedRoleResponseDTO)).thenReturn(role);
        when(roleMapper.roleToRoleResponseDto(savedRole)).thenReturn(expectedRoleResponseDTO);

        rolesService.deleteRole(idRole);
        Mockito.verify(roleRepository, Mockito.times(1)).deleteById(idRole);
    }


    @Test
    void testGetRoles() {
        when(roleRepository.findAll()).thenReturn(rolesList);
        when(roleMapper.roleToRoleResponseDto(any())).thenReturn(rolesResponseList.get(0), rolesResponseList.get(1));

        Collection<RoleResponseDTO> result = rolesService.getRoles();

        assertNotNull(result);
        assertEquals(rolesList.size(), result.size());

        List<RoleResponseDTO> resultList = new ArrayList<>(result);
        assertEquals(rolesResponseList.get(0), resultList.get(0));
        assertEquals(rolesResponseList.get(1), resultList.get(1));
    }

    @Test
    void getRoleByIdWhenValidIdShouldReturnRoleResponseDto() {
        Long idRole = 1L;

        when(roleRepository.findById(idRole)).thenReturn(Optional.of(role));
        when(roleMapper.roleDtoToRole(expectedRoleResponseDTO)).thenReturn(role);
        when(roleMapper.roleToRoleResponseDto(role)).thenReturn(expectedRoleResponseDTO);

        RoleResponseDTO result = rolesService.getRoleById(idRole);
        assertEquals(idRole, result.getIdRole());
    }

    @Test
    void getRoleByIdWhenExceptionThrownShouldThrowException() {
        Long idRole = 1L;

        when(roleRepository.findById(idRole)).thenReturn(Optional.of(role));
        when(roleMapper.roleDtoToRole(expectedRoleResponseDTO)).thenReturn(role);
        when(roleMapper.roleToRoleResponseDto(role)).thenReturn(expectedRoleResponseDTO);

        when(roleRepository.findById(anyLong())).thenThrow(new RuntimeException("Exception occurred while fetch role from Database"));
        assertThrows(RolesServiceBusinessException.class, () -> {
            rolesService.getRoleById(idRole);
        });
    }


}
