package tn.sofrecom.servicegestionacces;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import tn.sofrecom.servicegestionacces.dto.roles.RoleResponseDTO;
import tn.sofrecom.servicegestionacces.dto.users.UserRequestDTO;
import tn.sofrecom.servicegestionacces.dto.users.UserResponseDTO;
import tn.sofrecom.servicegestionacces.handlers.users.UserServiceBusinessException;
import tn.sofrecom.servicegestionacces.mappers.RoleMapperImpl;
import tn.sofrecom.servicegestionacces.mappers.UserMapperImpl;
import tn.sofrecom.servicegestionacces.model.Permission;
import tn.sofrecom.servicegestionacces.model.Role;
import tn.sofrecom.servicegestionacces.model.User;
import tn.sofrecom.servicegestionacces.repository.PermissionRepository;
import tn.sofrecom.servicegestionacces.repository.RoleRepository;
import tn.sofrecom.servicegestionacces.repository.UserRepository;
import tn.sofrecom.servicegestionacces.service.roles.implement.RolesServiceImp;
import tn.sofrecom.servicegestionacces.service.users.implement.UserServiceImp;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
class UserServiceImpTest {
    @Mock
    private UserRepository userRepository ;

    @Mock
    private UserMapperImpl userMapper;

    @Mock
    private  RolesServiceImp rolesService ;

    @Mock
    private RoleMapperImpl roleMapper;
    @Mock
    private RoleRepository roleRepository;
    @Mock
    private PermissionRepository permissionRepository;

    @InjectMocks
    private UserServiceImp userService;

    private UserRequestDTO userRequestDTO;
    private User user;
    private RoleResponseDTO roleResponseDTO ;
    private User savedUser;

    private Role role;
    private List<Permission> permissionsList;
    private UserResponseDTO expectedUserResponseDTO;
    private List<User> usersList;

    private List<UserResponseDTO> usersResponseList;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.openMocks(this);
        userRepository = Mockito.mock(UserRepository.class);
        roleMapper = Mockito.mock(RoleMapperImpl.class);
        userMapper = Mockito.mock(UserMapperImpl.class);
        userMapper = Mockito.mock(UserMapperImpl.class);
        roleRepository = Mockito.mock(RoleRepository.class);
        permissionRepository = Mockito.mock(PermissionRepository.class);
        rolesService = new RolesServiceImp(roleRepository, permissionRepository, roleMapper);
        userService = new UserServiceImp(userRepository,rolesService,userMapper,roleMapper,null ,null);

        userRequestDTO = new UserRequestDTO();
        userRequestDTO.setIdUser(1L);
        userRequestDTO.setFirstName("User name");
        userRequestDTO.setLastName("Last name ");
        userRequestDTO.setEmail("username.lastname@sofrecom.com");
        userRequestDTO.setPassword("password");
        userRequestDTO.setIdRole(1L);
        userRequestDTO.setIdCompany(1L);
        role = new Role();
        role.setIdRole(1L);
        role.setRoleName("Admin");
        role.setPermissions(permissionsList);

        permissionsList = new ArrayList<>();
        permissionsList.add(new Permission(1L, "read", null));
        permissionsList.add(new Permission(2L, "write", null));

        roleResponseDTO = new RoleResponseDTO();
        roleResponseDTO.setIdRole(1L);
        roleResponseDTO.setRoleName("Admin");
        roleResponseDTO.setPermissions(permissionsList);

        user = new User();
        user.setIdUser(1L);
        user.setFirstName("User name");
        user.setLastName("Last name ");
        user.setEmail("username.lastname@sofrecom.com");
        user.setPassword("password");
        user.setIdRole(1L);
        user.setIdCompany(1L);
        user.setRole(role);

        savedUser = new User();
        savedUser.setIdUser(1L);
        savedUser.setFirstName("User name");
        savedUser.setLastName("Last name ");
        savedUser.setEmail("username.lastname@sofrecom.com");
        savedUser.setPassword("password");
        savedUser.setIdRole(1L);
        savedUser.setIdCompany(1L);
        savedUser.setRole(role);

        expectedUserResponseDTO = new UserResponseDTO();
        expectedUserResponseDTO.setIdUser(1L);
        expectedUserResponseDTO.setFirstName("User name");
        expectedUserResponseDTO.setLastName("Last name ");
        expectedUserResponseDTO.setEmail("username.lastname@sofrecom.com");
        expectedUserResponseDTO.setPassword("password");
        expectedUserResponseDTO.setIdCompany(1L);
        expectedUserResponseDTO.setRole(role);
        usersList = new ArrayList<>();
        usersList.add(new User(1L,"User name","Last name ","username.lastname@sofrecom.com","password",1L,1L,role));
        usersList.add(new User(2L,"User name","Last name ","username.lastname@sofrecom.com","password",1L,1L,role));

        usersResponseList = new ArrayList<>();
        usersResponseList.add(new UserResponseDTO(1L,"User name","Last name ","username.lastname@sofrecom.com","password",1L,role));
        usersResponseList.add(new UserResponseDTO(2L,"User name","Last name ","username.lastname@sofrecom.com","password",1L,role));


    }
    @Test
    void testCreateNewUser() {
        when(userMapper.userDtoToUser(userRequestDTO)).thenReturn(user);
        when(roleRepository.findById(userRequestDTO.getIdRole())).thenReturn(Optional.of(role));
        when(rolesService.getRoleById(user.getIdRole())).thenReturn(roleResponseDTO);
        when(roleMapper.roleDtoToRole(roleResponseDTO)).thenReturn(role);
        when(userRepository.saveAndFlush(user)).thenReturn(savedUser);
        when(userMapper.userToUserResponseDto(savedUser)).thenReturn(expectedUserResponseDTO);

        UserResponseDTO actualUserResponseDTO = userService.createNewUser(userRequestDTO);
        assertNotNull(actualUserResponseDTO);
        assertEquals(actualUserResponseDTO.getIdUser(), expectedUserResponseDTO.getIdUser());
        assertEquals(actualUserResponseDTO.getRole(), expectedUserResponseDTO.getRole());

    }

    @Test
    void testUpdateUser() {
        when(userRepository.findById(userRequestDTO.getIdUser())).thenReturn(Optional.ofNullable(user));
        when(userService.getUserById(userRequestDTO.getIdUser())).thenReturn(expectedUserResponseDTO);
        when(userMapper.userDtoToUser(expectedUserResponseDTO)).thenReturn(user);
        when(roleRepository.findById(userRequestDTO.getIdRole())).thenReturn(Optional.of(role));
        when(rolesService.getRoleById(user.getIdRole())).thenReturn(roleResponseDTO);

        when(roleMapper.roleDtoToRole(roleResponseDTO)).thenReturn(role);
        when(userRepository.saveAndFlush(user)).thenReturn(savedUser);
        when(userMapper.userToUserResponseDto(savedUser)).thenReturn(expectedUserResponseDTO);

        UserResponseDTO UpdateUserResponseDTO = userService.updateUser(userRequestDTO);
        assertNotNull(UpdateUserResponseDTO);
        assertEquals(UpdateUserResponseDTO.getIdUser(), expectedUserResponseDTO.getIdUser());
        assertEquals(UpdateUserResponseDTO.getRole(), expectedUserResponseDTO.getRole());



    }

    @Test
    void testDeleteUser() {
        Long idUser = 1L;
        when(userRepository.findById(idUser)).thenReturn(Optional.of(user));
        when(userMapper.userDtoToUser(expectedUserResponseDTO)).thenReturn(user);
        when(userMapper.userToUserResponseDto(user)).thenReturn(expectedUserResponseDTO);

        userService.deleteUser(idUser);
        Mockito.verify(userRepository, Mockito.times(1)).deleteById(idUser);
    }

    @Test
    void testGetUsers() {
        when(userRepository.findAll()).thenReturn(usersList);
        when(userMapper.userToUserResponseDto(any())).thenReturn(usersResponseList.get(0), usersResponseList.get(1));

        Collection<UserResponseDTO> result = userService.getUsers();

        assertNotNull(result);
        assertEquals(usersResponseList.size(), result.size());

        List<UserResponseDTO> resultList = new ArrayList<>(result);
        assertEquals(usersResponseList.get(0), resultList.get(0));
        assertEquals(usersResponseList.get(1), resultList.get(1));
    }


    @Test
    void getUserByIdWhenValidIdShouldReturnUserResponseDto() {
        Long idUser = 1L;

        when(userRepository.findById(idUser)).thenReturn(Optional.of(user));
        when(userMapper.userDtoToUser(expectedUserResponseDTO)).thenReturn(user);
        when(userMapper.userToUserResponseDto(user)).thenReturn(expectedUserResponseDTO);

        UserResponseDTO result = userService.getUserById(idUser);
        assertEquals(idUser, result.getIdUser());
    }

    @Test
    void getUserByIdWhenExceptionThrownShouldThrowException() {
        Long idUser = 1L;

        when(userRepository.findById(idUser)).thenReturn(Optional.of(user));
        when(userMapper.userDtoToUser(expectedUserResponseDTO)).thenReturn(user);
        when(userMapper.userToUserResponseDto(user)).thenReturn(expectedUserResponseDTO);

        when(userRepository.findById(anyLong())).thenThrow(new RuntimeException("Exception occurred while fetch user from Database"));
        assertThrows(UserServiceBusinessException.class, () -> {
            userService.getUserById(idUser);
        });
    }



}
