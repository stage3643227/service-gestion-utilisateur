package tn.sofrecom.servicegestionacces;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import tn.sofrecom.servicegestionacces.dto.permissions.PermissionsRequestDTO;
import tn.sofrecom.servicegestionacces.dto.permissions.PermissionsResponseDTO;
import tn.sofrecom.servicegestionacces.handlers.permissions.PermissionsServiceBusinessException;
import tn.sofrecom.servicegestionacces.mappers.PermissionMapper;
import tn.sofrecom.servicegestionacces.mappers.PermissionMapperImpl;
import tn.sofrecom.servicegestionacces.model.Permission;
import tn.sofrecom.servicegestionacces.repository.PermissionRepository;
import tn.sofrecom.servicegestionacces.service.permissions.implement.PermissionServiceImp;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.when;


@RunWith(MockitoJUnitRunner.class)
class PermissionServiceImpTests {
    @Mock
    private PermissionRepository permissionRepository;

    @Mock
    private PermissionMapper permissionMapper;

    @InjectMocks
    private PermissionServiceImp permissionService;

    private PermissionsRequestDTO permissionsRequestDTO;
    private Permission permission;
    private Permission savedPermission;
    private PermissionsResponseDTO expectedPermissionsResponseDTO;
    private List<Permission> permissionsList;

    private List<PermissionsResponseDTO> permissionResponseList;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.openMocks(this);
        permissionRepository = Mockito.mock(PermissionRepository.class);
        permissionMapper = Mockito.mock(PermissionMapperImpl.class);
        permissionService = new PermissionServiceImp(permissionRepository, permissionMapper);

        permissionsRequestDTO = new PermissionsRequestDTO();
        permissionsRequestDTO.setIdPermission(1L);
        permissionsRequestDTO.setPermissionName("read");


        permission = new Permission();
        permission.setIdPermission(1L);
        permission.setPermissionName("read");

        savedPermission = new Permission();
        savedPermission.setIdPermission(1L);
        savedPermission.setPermissionName("read");

        expectedPermissionsResponseDTO = new PermissionsResponseDTO();
        expectedPermissionsResponseDTO.setIdPermission(1L);
        expectedPermissionsResponseDTO.setPermissionName("read");


        permissionsList = new ArrayList<>();
        permissionsList.add(new Permission(1L, "read", null));
        permissionsList.add(new Permission(2L, "write", null));
        permissionResponseList = new ArrayList<>();
        permissionResponseList.add(new PermissionsResponseDTO(1L, "read"));
        permissionResponseList.add(new PermissionsResponseDTO(2L, "read"));
    }

    @Test
    void testCreateNewPermission() {
        when(permissionMapper.permissionDtoToPermission(permissionsRequestDTO)).thenReturn(permission);
        when(permissionRepository.saveAndFlush(permission)).thenReturn(savedPermission);
        when(permissionMapper.permissionToPermissionResponseDto(savedPermission)).thenReturn(expectedPermissionsResponseDTO);

        PermissionsResponseDTO actualPermissionResponseDTO = permissionService.createNewPermission(permissionsRequestDTO);
        assertNotNull(actualPermissionResponseDTO);
        assertEquals(expectedPermissionsResponseDTO.getIdPermission(), actualPermissionResponseDTO.getIdPermission());
        assertEquals(expectedPermissionsResponseDTO.getPermissionName(), actualPermissionResponseDTO.getPermissionName());
    }

    @Test
    void testUpdatePermission() {

        when(permissionRepository.findById(permissionsRequestDTO.getIdPermission())).thenReturn(Optional.of(permission));
        when(permissionMapper.permissionDtoToPermission(expectedPermissionsResponseDTO)).thenReturn(permission);
        when(permissionMapper.permissionDtoToPermission(permissionsRequestDTO)).thenReturn(permission);
        when(permissionRepository.saveAndFlush(permission)).thenReturn(savedPermission);
        when(permissionMapper.permissionToPermissionResponseDto(savedPermission)).thenReturn(expectedPermissionsResponseDTO);

        PermissionsResponseDTO actualPermissionResponseDTO = permissionService.updatePermission(permissionsRequestDTO);
        assertNotNull(actualPermissionResponseDTO);
        assertEquals(expectedPermissionsResponseDTO.getIdPermission(), actualPermissionResponseDTO.getIdPermission());
        assertEquals(expectedPermissionsResponseDTO.getPermissionName(), actualPermissionResponseDTO.getPermissionName());

    }

    @Test
    void testDeletePermission() {
        Long idPermission = 1L;
        when(permissionRepository.findById(permissionsRequestDTO.getIdPermission())).thenReturn(Optional.of(permission));
        when(permissionMapper.permissionDtoToPermission(expectedPermissionsResponseDTO)).thenReturn(permission);
        when(permissionMapper.permissionToPermissionResponseDto(savedPermission)).thenReturn(expectedPermissionsResponseDTO);

        permissionService.deletePermission(idPermission);
        Mockito.verify(permissionRepository, Mockito.times(1)).deleteById(idPermission);
    }

    @Test
    void getPermissionByIdWhenValidIdShouldReturnPermissionResponseDto() {
        Long idPermission = 1L;

        when(permissionRepository.findById(idPermission)).thenReturn(Optional.of(permission));
        when(permissionMapper.permissionDtoToPermission(expectedPermissionsResponseDTO)).thenReturn(permission);
        when(permissionMapper.permissionToPermissionResponseDto(permission)).thenReturn(expectedPermissionsResponseDTO);

        PermissionsResponseDTO result = permissionService.getPermissionById(idPermission);
        assertEquals(idPermission, result.getIdPermission());
    }

    @Test
    void getPermissionByIdWhenExceptionThrownShouldThrowException() {
        Long idPermission = 1L;
        when(permissionRepository.findById(idPermission)).thenReturn(Optional.of(permission));
        when(permissionMapper.permissionDtoToPermission(expectedPermissionsResponseDTO)).thenReturn(permission);
        when(permissionMapper.permissionToPermissionResponseDto(permission)).thenReturn(expectedPermissionsResponseDTO);

        when(permissionRepository.findById(anyLong())).thenThrow(new RuntimeException("Exception occurred while fetch permission from Database"));
        assertThrows(PermissionsServiceBusinessException.class, () -> {
            permissionService.getPermissionById(idPermission);
        });
    }

    @Test
    void testGetPermissions() {
        when(permissionRepository.findAll()).thenReturn(permissionsList);
        when(permissionMapper.permissionToPermissionResponseDto(any())).thenReturn(permissionResponseList.get(0), permissionResponseList.get(1));

        Collection<PermissionsResponseDTO> result = permissionService.getPermissions();

        assertNotNull(result);
        assertEquals(permissionsList.size(), result.size());

        List<PermissionsResponseDTO> resultList = new ArrayList<>(result);
        assertEquals(permissionResponseList.get(0), resultList.get(0));
        assertEquals(permissionResponseList.get(1), resultList.get(1));
    }

}
