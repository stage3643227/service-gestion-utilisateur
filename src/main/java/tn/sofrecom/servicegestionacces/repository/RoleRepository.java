package tn.sofrecom.servicegestionacces.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import tn.sofrecom.servicegestionacces.model.Role;
@Repository
public interface RoleRepository extends JpaRepository<Role,Long> {
}
