package tn.sofrecom.servicegestionacces.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import org.springframework.stereotype.Repository;

import tn.sofrecom.servicegestionacces.model.Permission;
@Repository
public interface PermissionRepository extends JpaRepository<Permission,Long> {
}
