package tn.sofrecom.servicegestionacces.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import tn.sofrecom.servicegestionacces.model.User;
@Repository
public interface UserRepository extends JpaRepository<User,Long> {
    User findUserByEmail(String username);
}
