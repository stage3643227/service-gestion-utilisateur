package tn.sofrecom.servicegestionacces.mappers;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;
import tn.sofrecom.servicegestionacces.dto.users.UserRequestDTO;
import tn.sofrecom.servicegestionacces.dto.users.UserResponseDTO;
import tn.sofrecom.servicegestionacces.model.User;

@Mapper(componentModel = "spring")
public interface UserMapper {
    UserMapper  INSTANCE = Mappers.getMapper(UserMapper.class);
    User userDtoToUser(UserRequestDTO userRequestDTO);

    UserResponseDTO userToUserResponseDto(User user);
    User userDtoToUser(UserResponseDTO userResponseDTO);
}
