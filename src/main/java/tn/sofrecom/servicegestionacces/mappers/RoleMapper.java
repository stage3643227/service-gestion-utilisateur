package tn.sofrecom.servicegestionacces.mappers;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;
import tn.sofrecom.servicegestionacces.dto.roles.RoleRequestDTO;
import tn.sofrecom.servicegestionacces.dto.roles.RoleResponseDTO;
import tn.sofrecom.servicegestionacces.model.Role;

@Mapper(componentModel = "spring")
public interface RoleMapper {
    RoleMapper  INSTANCE = Mappers.getMapper(RoleMapper.class);
    Role roleDtoToRole(RoleRequestDTO roleRequestDTO);

    RoleResponseDTO roleToRoleResponseDto(Role role);
    Role roleDtoToRole(RoleResponseDTO roleResponseDTO);
}
