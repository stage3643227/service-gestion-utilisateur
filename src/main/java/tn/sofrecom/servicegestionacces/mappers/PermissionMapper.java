package tn.sofrecom.servicegestionacces.mappers;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;
import tn.sofrecom.servicegestionacces.dto.permissions.PermissionsRequestDTO;
import tn.sofrecom.servicegestionacces.dto.permissions.PermissionsResponseDTO;
import tn.sofrecom.servicegestionacces.model.Permission;

@Mapper(componentModel = "spring")
public interface PermissionMapper {
    PermissionMapper  INSTANCE = Mappers.getMapper(PermissionMapper.class);
    Permission permissionDtoToPermission(PermissionsRequestDTO permissionsRequestDTO);

    PermissionsResponseDTO permissionToPermissionResponseDto(Permission permission);
    Permission permissionDtoToPermission(PermissionsResponseDTO permissionsResponseDTO);
}
