package tn.sofrecom.servicegestionacces.controller.permissions.implement;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import tn.sofrecom.servicegestionacces.controller.permissions.PermissionsController;
import tn.sofrecom.servicegestionacces.dto.APIResponse;
import tn.sofrecom.servicegestionacces.dto.permissions.PermissionsRequestDTO;
import tn.sofrecom.servicegestionacces.dto.permissions.PermissionsResponseDTO;
import tn.sofrecom.servicegestionacces.service.permissions.implement.PermissionServiceImp;

import javax.validation.Valid;
import java.util.Collection;
import java.util.List;

/**
 * Implementation of the PermissionsController interface that handles incoming HTTP requests
 * for managing permissions.
 */
@RestController
@AllArgsConstructor
@Slf4j
@RequestMapping("/api/v1/Permissions")
@CrossOrigin(origins = "http://localhost:4200")
public class PermissionsControllerImp implements PermissionsController {
    public static final String SUCCESS = "Success";
    private PermissionServiceImp permissionServiceImp;

    /**
     * Creates a new permission based on the provided request data.
     *
     * @param permissionsRequestDTO the request data for creating the permission
     * @return a ResponseEntity containing the newly created permission
     */
    @Override
    public ResponseEntity<APIResponse> createNewPermission(@Valid @RequestBody PermissionsRequestDTO permissionsRequestDTO) {
        log.info("PermissionsController::createNewPermission request body {}", permissionsRequestDTO);

        PermissionsResponseDTO permissionsResponseDTO = permissionServiceImp.createNewPermission(permissionsRequestDTO);
        APIResponse<PermissionsResponseDTO> responseDTO = APIResponse
                .<PermissionsResponseDTO>builder()
                .status(SUCCESS)
                .results(permissionsResponseDTO)
                .build();

        log.info("PermissionsController::createNewPermission response {}", permissionsResponseDTO);
        return new ResponseEntity<>(responseDTO, HttpStatus.CREATED);
    }
    /**
     * Updates an existing permission based on the provided request data.
     *
     * @param permissionsRequestDTO the request data for updating the permission
     * @return a ResponseEntity containing the updated permission
     */
    @Override
    public ResponseEntity<APIResponse> updatePermission(@Valid @RequestBody PermissionsRequestDTO permissionsRequestDTO) {
        log.info("PermissionsController::updatePermission request body {}", permissionsRequestDTO);
        PermissionsResponseDTO permissionsResponseDTO = permissionServiceImp.updatePermission(permissionsRequestDTO);
        APIResponse<PermissionsResponseDTO> responseDTO = APIResponse
                .<PermissionsResponseDTO>builder()
                .status(SUCCESS)
                .results(permissionsResponseDTO)
                .build();

        log.info("PermissionsController::updatePermission response {}", permissionsResponseDTO);
        return new ResponseEntity<>(responseDTO, HttpStatus.ACCEPTED);
    }
    /**
     * Deletes a permission with the given ID.
     *
     * @param idPermission the ID of the permission to delete
     * @return a ResponseEntity indicating success or failure of the deletion
     */
    @Override
    public ResponseEntity<Void> deletePermission(Long idPermission) {
        log.info("PermissionsController::deletePermission by idPermission  {}", idPermission);
        permissionServiceImp.deletePermission(idPermission);
        return ResponseEntity.ok().build();
    }
    /**
     * Retrieves a permission with the given ID.
     *
     * @param idPermission the ID of the permission to retrieve
     * @return a ResponseEntity containing the retrieved permission
     */
    @Override
    public ResponseEntity<APIResponse> getPermissionById(Long idPermission) {
        log.info("PermissionsController::getPermissionById by idPermission  {}", idPermission);

        PermissionsResponseDTO permissionsResponseDTO = permissionServiceImp.getPermissionById(idPermission);
        APIResponse<PermissionsResponseDTO> responseDTO = APIResponse
                .<PermissionsResponseDTO>builder()
                .status(SUCCESS)
                .results(permissionsResponseDTO)
                .build();

        log.info("PermissionsController::getPermissionById by idPermission {} response {}", idPermission, permissionsResponseDTO);

        return new ResponseEntity<>(responseDTO, HttpStatus.OK);
    }
    /**
     * Retrieves all permissions.
     *
     * @return a ResponseEntity containing a list of all permissions
     */
    @Override
    public ResponseEntity<APIResponse> getAllPermissions() {
        Collection<PermissionsResponseDTO> permissions = permissionServiceImp.getPermissions();
        APIResponse<List<PermissionsResponseDTO>> responseDTO = APIResponse
                .<List<PermissionsResponseDTO>>builder()
                .status(SUCCESS)
                .results((List<PermissionsResponseDTO>) permissions)
                .build();

        log.info("PermissionsController::getAllPermissions response {}", responseDTO);

        return new ResponseEntity<>(responseDTO, HttpStatus.OK);



    }
}