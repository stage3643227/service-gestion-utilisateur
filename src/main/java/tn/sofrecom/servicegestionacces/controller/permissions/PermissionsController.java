package tn.sofrecom.servicegestionacces.controller.permissions;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import tn.sofrecom.servicegestionacces.dto.APIResponse;
import tn.sofrecom.servicegestionacces.dto.permissions.PermissionsRequestDTO;
/**
 * Defines the interface for managing Permissions.
 */
public interface PermissionsController {

    @PostMapping
    ResponseEntity<APIResponse> createNewPermission(PermissionsRequestDTO permissionsRequestDTO);
    @PutMapping
    ResponseEntity<APIResponse> updatePermission(PermissionsRequestDTO permissionsRequestDTO);
    @DeleteMapping("/{idPermission}")
    ResponseEntity<Void> deletePermission(@PathVariable Long idPermission);
    @GetMapping("/{idPermission}")
    ResponseEntity<APIResponse> getPermissionById(@PathVariable Long idPermission);
    @GetMapping("/all")
    ResponseEntity<APIResponse> getAllPermissions();
}
