package tn.sofrecom.servicegestionacces.controller.roles;

import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import tn.sofrecom.servicegestionacces.dto.APIResponse;
import tn.sofrecom.servicegestionacces.dto.roles.RoleRequestDTO;
/**
 * Defines the interface for managing Roles .
 */
public interface RoleController {
    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity<APIResponse> createNewRole(RoleRequestDTO roleRequestDTO);
    @PutMapping
    ResponseEntity<APIResponse> updateRole(RoleRequestDTO roleRequestDTO);
    @DeleteMapping("/{idRole}")
    ResponseEntity<Void> deleteRole(@PathVariable Long idRole);
    @GetMapping("/{idRole}")
    ResponseEntity<APIResponse> getRoleById(@PathVariable Long idRole);
    @GetMapping("/all")
    ResponseEntity<APIResponse> getAllRoles();

}
