package tn.sofrecom.servicegestionacces.controller.roles.implement;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import tn.sofrecom.servicegestionacces.controller.roles.RoleController;
import tn.sofrecom.servicegestionacces.dto.APIResponse;
import tn.sofrecom.servicegestionacces.dto.roles.RoleRequestDTO;
import tn.sofrecom.servicegestionacces.dto.roles.RoleResponseDTO;
import tn.sofrecom.servicegestionacces.service.roles.implement.RolesServiceImp;

import javax.validation.Valid;
import java.util.Collection;
import java.util.List;
/**
 * Implementation of the RoleController interface that handles incoming HTTP requests for managing roles.
 */
@RestController
@AllArgsConstructor
@Slf4j
@RequestMapping("/api/v1/Roles")
@CrossOrigin(origins = "http://localhost:4200")
public class RolesControllerImp implements RoleController {

    public static final String SUCCESS = "Success";
    private RolesServiceImp rolesServiceImp;

    /**
     * Creates a new role based on the provided request data.
     *
     * @param roleRequestDTO the request data for creating the role
     * @return a ResponseEntity containing the newly created role
     */
    @Override
    public ResponseEntity<APIResponse> createNewRole(@Valid @RequestBody RoleRequestDTO roleRequestDTO) {
        log.info("RoleController::createNewRole request body {}", roleRequestDTO);

        RoleResponseDTO roleResponseDTO = rolesServiceImp.createNewRole(roleRequestDTO);
        APIResponse<RoleResponseDTO> responseDTO = APIResponse
                .<RoleResponseDTO>builder()
                .status(SUCCESS)
                .results(roleResponseDTO)
                .build();

        log.info("RoleController::createNewRole response {}", roleResponseDTO);
        return new ResponseEntity<>(responseDTO, HttpStatus.CREATED);
    }
    /**
     * Updates an existing role based on the provided request data.
     *
     * @param roleRequestDTO the request data for updating the role
     * @return a ResponseEntity containing the updated role
     */
    @Override
    public ResponseEntity<APIResponse> updateRole(@Valid @RequestBody RoleRequestDTO roleRequestDTO) {
        log.info("RoleController::updateRole request body {}", roleRequestDTO);
        RoleResponseDTO roleResponseDTO = rolesServiceImp.updateRole(roleRequestDTO);
        APIResponse<RoleResponseDTO> responseDTO = APIResponse
                .<RoleResponseDTO>builder()
                .status(SUCCESS)
                .results(roleResponseDTO)
                .build();

        log.info("RoleController::updateRole response {}", roleResponseDTO );
        return new ResponseEntity<>(responseDTO, HttpStatus.ACCEPTED);
    }
    /**
     * Deletes a role with the given ID.
     *
     * @param idRole the ID of the role to delete
     * @return a ResponseEntity indicating success or failure of the deletion
     */
    @Override
    public ResponseEntity<Void> deleteRole(Long idRole) {
        log.info("RoleController::deleteRole by idPermission  {}", idRole);
        rolesServiceImp.deleteRole(idRole);
        return ResponseEntity.ok().build();
    }
    /**
     * Retrieves a role with the given ID.
     *
     * @param idRole the ID of the role to retrieve
     * @return a ResponseEntity containing the retrieved role
     */
    @Override
    public ResponseEntity<APIResponse> getRoleById(Long idRole) {
        log.info("RoleController::getRoleById by idRole  {}", idRole);

        RoleResponseDTO roleResponseDTO = rolesServiceImp.getRoleById(idRole);
        APIResponse<RoleResponseDTO> responseDTO = APIResponse
                .<RoleResponseDTO>builder()
                .status(SUCCESS)
                .results(roleResponseDTO)
                .build();

        log.info("RoleController::getRoleById by idRole {} response {}", idRole, roleResponseDTO);

        return new ResponseEntity<>(responseDTO, HttpStatus.OK);
    }
    /**
     * Retrieves all roles.
     *
     * @return a ResponseEntity containing a list of all roles
     */
    @Override
    public ResponseEntity<APIResponse> getAllRoles() {
        Collection<RoleResponseDTO> roleResponseDTOS = rolesServiceImp.getRoles();
        APIResponse<List<RoleResponseDTO>> responseDTO = APIResponse
                .<List<RoleResponseDTO>>builder()
                .status(SUCCESS)
                .results((List<RoleResponseDTO>) roleResponseDTOS)
                .build();

        log.info("PermissionsController::getAllPermissions response {}", responseDTO);

        return new ResponseEntity<>(responseDTO, HttpStatus.OK);
    }
}
