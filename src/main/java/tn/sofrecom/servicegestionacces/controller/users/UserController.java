package tn.sofrecom.servicegestionacces.controller.users;


import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import tn.sofrecom.servicegestionacces.dto.APIResponse;
import tn.sofrecom.servicegestionacces.dto.users.UserRequestDTO;
/**
 * Defines the interface for managing Users .
 */
public interface UserController {
    @PostMapping
    ResponseEntity<APIResponse> createNewUser(UserRequestDTO userRequestDTO);
    @PutMapping
    ResponseEntity<APIResponse> updateUser(UserRequestDTO userRequestDTO);
    @DeleteMapping("/{idUser}")
    ResponseEntity<Void> deleteUser(@PathVariable Long idUser);
    @GetMapping("/{idUser}")
    ResponseEntity<APIResponse> getUserById(@PathVariable Long idUser);
    @GetMapping("/all")
    ResponseEntity<APIResponse> getAllUsers();
}
