package tn.sofrecom.servicegestionacces.controller.users.implement;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import tn.sofrecom.servicegestionacces.controller.users.UserController;
import tn.sofrecom.servicegestionacces.dto.APIResponse;
import tn.sofrecom.servicegestionacces.dto.users.AuthRequest;
import tn.sofrecom.servicegestionacces.dto.users.UserRequestDTO;
import tn.sofrecom.servicegestionacces.dto.users.UserResponseDTO;
import tn.sofrecom.servicegestionacces.service.users.implement.UserServiceImp;


import javax.validation.Valid;
import java.util.Collection;
import java.util.List;
/**
 * Implements the {@link UserController} interface.
 */
@RestController
@AllArgsConstructor
@Slf4j
@RequestMapping("/api/v1/Users")
@CrossOrigin(origins = "http://localhost:4200")
public class UserControllerImp implements UserController {
    public static final String SUCCESS = "Success";
    private UserServiceImp userServiceImp;


    /**
     * Creates a new user based on the provided request data.
     *
     * @param userRequestDTO the request data for creating the user
     * @return a ResponseEntity containing the newly created user
     */
    @Override
    public ResponseEntity<APIResponse> createNewUser(@Valid @RequestBody UserRequestDTO userRequestDTO) {
        log.info("UserController::createNewUser request body {}", userRequestDTO);
        UserResponseDTO userResponseDTO = userServiceImp.createNewUser(userRequestDTO);
        APIResponse<UserResponseDTO> responseDTO =   APIResponse
                .<UserResponseDTO>builder()
                .status(SUCCESS)
                .results(userResponseDTO)
                .build();

        log.info("UserController::createNewUser {}", userResponseDTO );
        return new ResponseEntity<>(responseDTO, HttpStatus.CREATED);
    }

    /**
     * Updates an existing user based on the provided request data.
     *
     * @param userRequestDTO the request data for updating the user
     * @return a ResponseEntity containing the updated user
     */
    @Override
    public ResponseEntity<APIResponse> updateUser(@Valid @RequestBody UserRequestDTO userRequestDTO) {
        log.info("UserController::createNewUser request body {}", userRequestDTO);
        UserResponseDTO userResponseDTO = userServiceImp.updateUser(userRequestDTO);
        APIResponse<UserResponseDTO> responseDTO = APIResponse
                .<UserResponseDTO>builder()
                .status(SUCCESS)
                .results(userResponseDTO)
                .build();

        log.info("UserController::createNewUser response {}", userRequestDTO );
        return new ResponseEntity<>(responseDTO, HttpStatus.ACCEPTED);
    }
    /**
     * Deletes a user with the given ID.
     *
     * @param idUser the ID of the user to delete
     * @return a ResponseEntity indicating success or failure of the deletion
     */
    @Override
    public ResponseEntity<Void> deleteUser(Long idUser) {
        log.info("UserController::deleteUser by idTopology  {}", idUser);
        userServiceImp.deleteUser(idUser);
        return  ResponseEntity.ok().build();
    }
    /**
     * Retrieves a user with the given ID.
     *
     * @param idUser the ID of the user to retrieve
     * @return a ResponseEntity containing the retrieved user
     */
    @Override
    public ResponseEntity<APIResponse> getUserById(Long idUser) {
        log.info("UserController::getUserById by idUser  {}", idUser);
        UserResponseDTO userResponseDTO = userServiceImp.getUserById(idUser);
        APIResponse<UserResponseDTO> responseDTO = APIResponse
                .<UserResponseDTO>builder()
                .status(SUCCESS)
                .results(userResponseDTO)
                .build();

        log.info("UserController::getUserById by idUser {} response {}", idUser,userResponseDTO);

        return new ResponseEntity<>(responseDTO, HttpStatus.OK);
    }
    /**
     * Retrieves all users.
     *
     * @return a ResponseEntity containing a list of all users
     */
    @Override
    public ResponseEntity<APIResponse> getAllUsers() {
        Collection<UserResponseDTO> userResponseDTOS = userServiceImp.getUsers();
        APIResponse<List<UserResponseDTO>> responseDTO = APIResponse
                .<List<UserResponseDTO>>builder()
                .status(SUCCESS)
                .results((List<UserResponseDTO>) userResponseDTOS)
                .build();

        log.info("UserController::getUsers response {}", responseDTO);

        return new ResponseEntity<>(responseDTO, HttpStatus.OK);
    }
    /**
     * Authenticates a user with the provided email and password.
     *
     * @param authRequest the authentication request containing the user's email and password
     * @return a ResponseEntity containing the authenticated user
     */
    @PostMapping("/Login")
    ResponseEntity<APIResponse> login(@Valid @RequestBody AuthRequest authRequest){
        log.info("UserController::Login by Email and Password  {}", authRequest);
        UserResponseDTO userResponseDTO = userServiceImp.login(authRequest);
        APIResponse<UserResponseDTO> responseDTO = APIResponse
                .<UserResponseDTO>builder()
                .status(SUCCESS)
                .results(userResponseDTO)
                .build();

        log.info("UserController::Login by Email and Password  {} response {}", authRequest,userResponseDTO);

        return new ResponseEntity<>(responseDTO, HttpStatus.OK);

    }
}
