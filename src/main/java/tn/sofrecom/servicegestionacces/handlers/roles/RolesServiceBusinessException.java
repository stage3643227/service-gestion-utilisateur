package tn.sofrecom.servicegestionacces.handlers.roles;

public class RolesServiceBusinessException extends RuntimeException {
    public RolesServiceBusinessException(String message) {
        super(message);
    }
}
