package tn.sofrecom.servicegestionacces.handlers.roles;

public class RolesNotFoundException extends RuntimeException{
    public RolesNotFoundException(String message) {
        super(message);
    }
}
