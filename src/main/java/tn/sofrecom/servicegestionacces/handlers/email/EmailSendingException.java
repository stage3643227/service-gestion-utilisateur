package tn.sofrecom.servicegestionacces.handlers.email;

public class EmailSendingException extends RuntimeException{
    public EmailSendingException(String message) {
        super(message);
    }
}
