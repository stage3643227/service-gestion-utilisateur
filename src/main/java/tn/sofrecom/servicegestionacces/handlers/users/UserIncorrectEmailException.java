package tn.sofrecom.servicegestionacces.handlers.users;

public class UserIncorrectEmailException extends RuntimeException{
    public UserIncorrectEmailException(String message) {
        super(message);
    }
}
