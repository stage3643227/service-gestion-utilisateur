package tn.sofrecom.servicegestionacces.handlers.users;

public class UserIncorrectPasswordException extends RuntimeException{
    public UserIncorrectPasswordException(String message) {
        super(message);
    }
}
