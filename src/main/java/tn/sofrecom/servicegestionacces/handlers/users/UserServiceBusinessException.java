package tn.sofrecom.servicegestionacces.handlers.users;

public class UserServiceBusinessException extends RuntimeException{
    public UserServiceBusinessException(String message) {
        super(message);
    }
}
