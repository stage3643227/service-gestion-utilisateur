package tn.sofrecom.servicegestionacces.handlers.users;

public class UserNotFoundExecption extends RuntimeException{
    public UserNotFoundExecption(String message) {
        super(message);
    }
}
