package tn.sofrecom.servicegestionacces.handlers.permissions;

public class PermissionsServiceBusinessException extends RuntimeException{
    public PermissionsServiceBusinessException(String message) {
        super(message);
    }
}
