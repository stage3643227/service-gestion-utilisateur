package tn.sofrecom.servicegestionacces.handlers.permissions;

public class PermissionsNotFoundException extends RuntimeException {
    public PermissionsNotFoundException(String message) {
        super(message);
    }
}
