package tn.sofrecom.servicegestionacces.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Collection;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
@Entity
@Table(name = "PERMISSIONS")
public class Permission implements Serializable {
    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    @Column(name="id_permission")
    private Long idPermission;

    @Column(name="permission_name")
    private String permissionName;

    @ManyToMany(mappedBy = "permissions")
    @JsonBackReference
    private Collection<Role> roles;


}
