package tn.sofrecom.servicegestionacces.dto.permissions;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
public class PermissionsRequestDTO {
    private Long idPermission;
    @NotBlank(message = "Permission name shouldn't be NULL OR EMPTY")
    private String permissionName;
}
