package tn.sofrecom.servicegestionacces.dto.users;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
public class UserRequestDTO {
    private long idUser;
    @NotBlank(message = "First name shouldn't be NULL OR EMPTY")
    private String firstName;
    @NotBlank(message = "Last name shouldn't be NULL OR EMPTY")
    private String lastName;
    @NotBlank(message = "Password name shouldn't be NULL OR EMPTY")
    private String password;
    @NotBlank(message = "Email name shouldn't be NULL OR EMPTY")
    private String email;
    @NotNull(message = "Company id shouldn't be NULL")
    private Long idCompany;
    @NotNull(message = "Role id shouldn't be NULL")
    private Long idRole;
}
