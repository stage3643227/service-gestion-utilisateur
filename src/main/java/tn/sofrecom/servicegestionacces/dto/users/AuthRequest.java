package tn.sofrecom.servicegestionacces.dto.users;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
public class AuthRequest {
    @NotBlank(message = "UserName name shouldn't be NULL OR EMPTY")
    private String userName;
    @NotBlank(message = "Password name shouldn't be NULL OR EMPTY")
    private String password;
}
