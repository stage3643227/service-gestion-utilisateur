package tn.sofrecom.servicegestionacces.dto.users;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import tn.sofrecom.servicegestionacces.model.Role;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
@JsonInclude(JsonInclude.Include.NON_NULL)
public class UserResponseDTO {
    private long idUser;
    private String firstName;
    private String lastName;
    private String password;
    private String email;
    private Long idCompany;
    private Role role;
}
