package tn.sofrecom.servicegestionacces.dto.roles;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import tn.sofrecom.servicegestionacces.model.Permission;

import java.util.Collection;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
@JsonInclude(JsonInclude.Include.NON_NULL)
public class RoleResponseDTO {
    private long idRole;
    private String roleName;
    private Collection<Permission> permissions;
}
