package tn.sofrecom.servicegestionacces.dto.roles;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.util.List;
@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
public class RolePermissionRequestDTO {
    @NotNull(message = "Role id shouldn't be NULL")
    private Long idRole;
    private List<Long> permissionIds;
}
