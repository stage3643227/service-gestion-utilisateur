package tn.sofrecom.servicegestionacces.dto.roles;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
public class RoleRequestDTO {
    private long idRole;
    @NotBlank(message = "Role name shouldn't be NULL OR EMPTY")
    private String roleName;
    private List<Long> permissionIds;
}
