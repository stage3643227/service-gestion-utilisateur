package tn.sofrecom.servicegestionacces.service.permissions;

import tn.sofrecom.servicegestionacces.dto.permissions.PermissionsRequestDTO;
import tn.sofrecom.servicegestionacces.dto.permissions.PermissionsResponseDTO;

import java.util.Collection;

public interface PermissionService {
    PermissionsResponseDTO createNewPermission(PermissionsRequestDTO permissionsRequestDTO) ;
    PermissionsResponseDTO updatePermission(PermissionsRequestDTO permissionRequestDTO);
    void deletePermission(Long idPermission);
    Collection<PermissionsResponseDTO> getPermissions();
    PermissionsResponseDTO getPermissionById(Long idPermission);
}
