package tn.sofrecom.servicegestionacces.service.permissions.implement;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tn.sofrecom.servicegestionacces.dto.permissions.PermissionsRequestDTO;
import tn.sofrecom.servicegestionacces.dto.permissions.PermissionsResponseDTO;
import tn.sofrecom.servicegestionacces.handlers.permissions.PermissionsNotFoundException;
import tn.sofrecom.servicegestionacces.handlers.permissions.PermissionsServiceBusinessException;
import tn.sofrecom.servicegestionacces.mappers.PermissionMapper;
import tn.sofrecom.servicegestionacces.model.Permission;
import tn.sofrecom.servicegestionacces.repository.PermissionRepository;
import tn.sofrecom.servicegestionacces.service.permissions.PermissionService;

import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
@Slf4j
@Transactional
public class PermissionServiceImp implements PermissionService {
    private final PermissionRepository permissionRepository;
    private final PermissionMapper permissionMapper;
    /**
     * Creates a new permission.
     *
     * @param permissionsRequestDTO the DTO containing the permission data
     * @return the DTO containing the created permission data
     * @throws PermissionsServiceBusinessException if there is an error creating the permission
     */
    @Override
    public PermissionsResponseDTO createNewPermission(PermissionsRequestDTO permissionsRequestDTO) {
        PermissionsResponseDTO permissionsResponseDTO;

        try {
            log.info("PermissionService:createNewPermission execution started.");
            Permission permission = permissionMapper.permissionDtoToPermission(permissionsRequestDTO);
            log.debug("PermissionService:createNewPermission request parameters {}", permission);

            Permission permissionResults = permissionRepository.saveAndFlush(permission);
            permissionsResponseDTO = permissionMapper.permissionToPermissionResponseDto(permissionResults);
            log.debug("PermissionService:createNewPermission received response from Database {}", permissionResults);

        } catch (Exception ex) {
            log.error("Exception occurred while persisting Permission to database , Exception message {}", ex.getMessage());
            throw new PermissionsServiceBusinessException("Exception occurred while create a new Permission");
        }
        log.info("PermissionService:createNewPermission execution ended.");
        return permissionsResponseDTO;

    }
    /**
     * Updates an existing permission.
     *
     * @param permissionRequestDTO the DTO containing the permission data
     * @return the DTO containing the updated permission data
     * @throws PermissionsServiceBusinessException if there is an error updating the permission
     */
    @Override
    public PermissionsResponseDTO updatePermission(PermissionsRequestDTO permissionRequestDTO) {
        PermissionsResponseDTO permissionsResponseDTO;
        Permission permission ;
        try {
            permission = permissionMapper.permissionDtoToPermission(getPermissionById(permissionRequestDTO.getIdPermission()));
            log.info("PermissionService:updatePermission execution started.");
            permission.setPermissionName(permissionMapper.permissionDtoToPermission(permissionRequestDTO).getPermissionName());
            permissionsResponseDTO = permissionMapper.permissionToPermissionResponseDto(permissionRepository.saveAndFlush(permission));
        } catch (Exception ex) {
            log.error("Exception occurred while persisting Permission to database , Exception message {}", ex.getMessage());
            throw new PermissionsServiceBusinessException("Exception occurred while update a Permission");
        }
        return permissionsResponseDTO;
    }
    /**
     * Deletes a permission by ID.
     *
     * @param idPermission the ID of the permission to delete
     * @throws PermissionsServiceBusinessException if there is an error deleting the permission
     */
    @Override
    public void deletePermission(Long idPermission) {
        log.debug("Request to delete Permission: {}", idPermission);
        if (getPermissionById(idPermission) != null)
            permissionRepository.deleteById(idPermission);
        else {
            throw new PermissionsServiceBusinessException("Exception occurred while delete a Permission");
        }

    }
    /**
     * Retrieves all permissions.
     *
     * @return a collection of DTOs containing the permissions data
     * @throws PermissionsServiceBusinessException if there is an error retrieving the permissions
     */
    @Override
    public Collection<PermissionsResponseDTO> getPermissions() {

        Collection<PermissionsResponseDTO> permissionsResponseDTOS = null;
        try {
            log.info("PermissionService:getPermissions execution started.");
            Collection<Permission> permissionList = permissionRepository.findAll();

            if (!permissionList.isEmpty()) {
                permissionsResponseDTOS = permissionList.stream().sorted(Comparator.comparing(Permission::getIdPermission))
                        .map(permissionMapper::permissionToPermissionResponseDto)
                        .collect(Collectors.toList());
            } else {
                permissionsResponseDTOS = Collections.emptyList();
            }
            log.debug("PermissionService:getPermissions retrieving Permissions from database  {}", permissionsResponseDTOS);

        } catch (Exception ex) {
            log.error("Exception occurred while retrieving Permissions from database , Exception message {}", ex.getMessage());
            throw new PermissionsServiceBusinessException("Exception occurred while fetch all Permissions from Database");
        }

        log.info("PermissionService:getPermissions  execution ended.");
        return permissionsResponseDTOS;
    }
    /**
     * Retrieves a permission by ID.
     *
     * @param idPermission the ID of the permission to retrieve
     * @return the DTO containing the permission data
     * @throws PermissionsNotFoundException        if the permission is not found
     * @throws PermissionsServiceBusinessException if there is an error retrieving the permission
     */
    @Override
    @Transactional(
            readOnly = true
    )
    public PermissionsResponseDTO getPermissionById(Long idPermission) {
        PermissionsResponseDTO permissionsResponseDTO;
        try {
            log.info("PermissionService:getPermissionById execution started.");
            Permission permission  = permissionRepository.findById(idPermission)
                    .orElseThrow(() -> new PermissionsNotFoundException("Permission not found with id " + idPermission));
            permissionsResponseDTO = permissionMapper.permissionToPermissionResponseDto(permission);
            log.debug("PermissionService:getPermissionById retrieving Permission  from database for id {} {}", idPermission, permissionsResponseDTO);

        } catch (Exception ex) {
            log.error("Exception occurred while retrieving Permission {} from database , Exception message {}", idPermission, ex.getMessage());
            throw new PermissionsServiceBusinessException("Exception occurred while fetch Permission from Database " + idPermission);
        }

        log.info("PermissionService:getPermissionById execution ended.");
        return permissionsResponseDTO;
    }
}
