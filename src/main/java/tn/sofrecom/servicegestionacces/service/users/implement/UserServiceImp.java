package tn.sofrecom.servicegestionacces.service.users.implement;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import org.springframework.context.annotation.DependsOn;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tn.sofrecom.servicegestionacces.dto.roles.RoleResponseDTO;
import tn.sofrecom.servicegestionacces.dto.users.AuthRequest;
import tn.sofrecom.servicegestionacces.dto.users.UserRequestDTO;
import tn.sofrecom.servicegestionacces.dto.users.UserResponseDTO;
import tn.sofrecom.servicegestionacces.handlers.users.UserIncorrectEmailException;
import tn.sofrecom.servicegestionacces.handlers.users.UserIncorrectPasswordException;
import tn.sofrecom.servicegestionacces.handlers.users.UserNotFoundExecption;
import tn.sofrecom.servicegestionacces.handlers.users.UserServiceBusinessException;
import tn.sofrecom.servicegestionacces.mappers.RoleMapper;
import tn.sofrecom.servicegestionacces.mappers.UserMapper;
import tn.sofrecom.servicegestionacces.model.User;
import tn.sofrecom.servicegestionacces.repository.UserRepository;
import tn.sofrecom.servicegestionacces.service.roles.implement.RolesServiceImp;
import tn.sofrecom.servicegestionacces.service.users.UserService;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
@DependsOn("securityConfig")
@Slf4j
@Transactional
public class UserServiceImp implements UserService {
    private final UserRepository userRepository ;
    private  final RolesServiceImp rolesServiceImp ;
    private final UserMapper userMapper ;
    private final RoleMapper roleMapper ;
    private final EmailService emailService;

    private final BCryptPasswordEncoder passwordEncoder;
    /**
     * Creates a new user.
     *
     * @param userRequestDTO user details
     * @return user details
     * @throws UserServiceBusinessException if any error occurs while creating the user
     */
    @Override
    public UserResponseDTO createNewUser(UserRequestDTO userRequestDTO) {
        UserResponseDTO userResponseDTO;


        try {
            log.info("UserService:createNewUser execution started.");
            User user = userMapper.userDtoToUser(userRequestDTO);
            log.info("UserService:createNewUser User {}.",user);
            String password = user.getPassword();
            RoleResponseDTO roleResponseDTO = rolesServiceImp.getRoleById(user.getIdRole());
            user.setRole(roleMapper.roleDtoToRole(roleResponseDTO));
            log.debug("UserService:createNewUser request parameters {}", user);
            user.setPassword(passwordEncoder.encode(user.getPassword()));
            User userResults = userRepository.saveAndFlush(user);
            String to = userResults.getEmail();
            String subject = " Informations sur votre nouveau compte ";
            String text = "Cher(e) " + userResults.getFirstName() + " " + userResults.getLastName() + ",\n\n" +
                     "Votre compte a été créé avec succès. Voici les informations concernant votre compte :\n\n" +
                    "Nom d'utilisateur : "+ userResults.getEmail() +" \n \n" +
                    "Mot de passe : "+password+"\n\n" +
                    "Si vous rencontrez des problèmes ou si vous avez des questions, n'hésitez pas à contacter notre équipe de support.\n\n" +
                    "Cordialement,\n\n" +
                    "L'équipe de support\n";
            emailService.sendSimpleMessage(to, subject, text);
            userResponseDTO  = userMapper.userToUserResponseDto(userResults);
            log.debug("UserService:createNewUser received response from Database {}", userResults);
        } catch (Exception ex) {
            log.error("Exception occurred while persisting User to database , Exception message {}", ex.getMessage());
            throw new UserServiceBusinessException("Exception occurred while create a new User");
        }
        log.info("UserService:createNewUser execution ended.");
        return userResponseDTO;
    }


    /**
     * Updates an existing user.
     *
     * @param userRequestDTO user details
     * @return user details
     * @throws UserServiceBusinessException if any error occurs while updating the user
     */
    @Override
    public UserResponseDTO updateUser(UserRequestDTO userRequestDTO) {
        UserResponseDTO userResponseDTO;
        User user ;
        try {
            user = userMapper.userDtoToUser(getUserById(userRequestDTO.getIdUser()));
            log.info("UserService:updateUser execution started.");
            user.setFirstName(userRequestDTO.getFirstName());
            user.setLastName(userRequestDTO.getLastName());
            user.setIdCompany(userRequestDTO.getIdCompany());
            user.setEmail(userRequestDTO.getEmail());
            RoleResponseDTO roleResponseDTO = rolesServiceImp.getRoleById(userRequestDTO.getIdRole());
            user.setRole(roleMapper.roleDtoToRole(roleResponseDTO));
            if (!passwordEncoder.matches(userRequestDTO.getPassword(), user.getPassword())) {
                String password = userRequestDTO.getPassword() ;
                user.setPassword(passwordEncoder.encode(userRequestDTO.getPassword()));
                userResponseDTO = userMapper.userToUserResponseDto(userRepository.saveAndFlush(user));
                String to = userRequestDTO.getEmail();
                String subject = "Changement de votre mot de passe";
                String text = "Cher(e) " + userResponseDTO.getFirstName() + " " + userResponseDTO.getLastName() + ",\n\n" +
                        "Nous souhaitons vous informer que votre mot de passe a été modifié avec succès. Voici les nouvelles informations de connexion à votre compte :\n\n" +
                        "Nom d'utilisateur : "+ userResponseDTO.getEmail()+" \n \n" +
                        "Nouveau mot de passe : "+password+"\n\n" +
                        "Cordialement,\n\n" +
                        "L'équipe de support\n";
                emailService.sendSimpleMessage(to, subject, text);
            }
            else {
                userResponseDTO = userMapper.userToUserResponseDto(userRepository.saveAndFlush(user));
            }

        } catch (Exception ex) {
            log.error("Exception occurred while persisting User to database , Exception message {}", ex.getMessage());
            throw new UserServiceBusinessException("Exception occurred while update a User");
        }
        return userResponseDTO;
    }
    /**
     * Deletes an existing user by ID.
     *
     * @param idUser user ID
     * @throws UserServiceBusinessException if any error occurs while deleting the user
     */
    @Override
    public void deleteUser(Long idUser) {
        log.debug("Request to delete user: {}", idUser);
        if (getUserById(idUser) != null)
            userRepository.deleteById(idUser);
        else {
            throw new UserServiceBusinessException("Exception occurred while delete a user");
        }
    }
    /**
     * Retrieves all users.
     *
     * @return a collection of {@link UserResponseDTO} objects
     * @throws UserServiceBusinessException if there is an exception while fetching users from the database
     */
    @Override
    public Collection<UserResponseDTO> getUsers() {
        Collection<UserResponseDTO> userResponseDTOS ;
        try {
            log.info("UserService:getUsers execution started.");
            Collection<User> userList = userRepository.findAll();

            if (!userList.isEmpty()) {
                userResponseDTOS = userList.stream().sorted(Comparator.comparing(User::getIdUser))
                        .map(userMapper::userToUserResponseDto)
                        .collect(Collectors.toList());
            } else {
                userResponseDTOS = Collections.emptyList();
            }
            log.debug("UserService:getUsers retrieving users from database  {}", userResponseDTOS);

        } catch (Exception ex) {
            log.error("Exception occurred while retrieving users from database , Exception message {}", ex.getMessage());
            throw new UserServiceBusinessException("Exception occurred while fetch all users from Database");
        }

        log.info("CompanyService:getCompanies execution ended.");
        return userResponseDTOS;
    }
    /**
     * Returns the user with the given ID.
     *
     * @param idUser the ID of the user to retrieve
     * @return the user with the given ID
     * @throws UserNotFoundExecption if no user is found with the given ID
     * @throws UserServiceBusinessException if an error occurs while retrieving the user
     */
    @Override
    public UserResponseDTO getUserById(Long idUser) {
        UserResponseDTO userResponseDTO;
        try {
            log.info("UserService::getUserById execution started.");
            User user = userRepository.findById(idUser)
                    .orElseThrow(() -> new UserNotFoundExecption("user not found with id " + idUser));
            userResponseDTO = userMapper.userToUserResponseDto(user);
            log.debug("UserService:: getUserById retrieving user  from database for idUser {} {}", idUser, userResponseDTO);

        } catch (Exception ex) {
            log.error("Exception occurred while retrieving user {} from database , Exception message {}", idUser, ex.getMessage());
            throw new UserServiceBusinessException("Exception occurred while fetch Topology from Database " + idUser);
        }

        log.info("UserService::getUserById execution ended.");
        return userResponseDTO;
    }
    /**
     * Authenticates a user based on the given credentials.
     *
     * @param authRequest the user's authentication credentials
     * @return the authenticated user
     * @throws UserNotFoundExecption if no user is found with the given email
     * @throws UserIncorrectPasswordException if the password is incorrect
     * @throws UserIncorrectEmailException if the email is incorrect
     * @throws UserServiceBusinessException if an error occurs while authenticating the user
     */
     public UserResponseDTO login (AuthRequest authRequest){
         UserResponseDTO userResponseDTO ;
         BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
         try {
             log.info("UserService::Login execution started.");
         Optional<User> user = Optional.ofNullable(userRepository.findUserByEmail(authRequest.getUserName()));
             if(user.isPresent()){
                if(user.get().getEmail().equals(authRequest.getUserName())) {
                    if (passwordEncoder.matches(authRequest.getPassword(), user.get().getPassword())) {
                        userResponseDTO = userMapper.userToUserResponseDto(user.get());
                        return userResponseDTO;
                    } else throw new UserIncorrectPasswordException("verify your password");
                }else throw new UserIncorrectEmailException("verify your email");
             }
             throw new UserNotFoundExecption("user not found with Email " + authRequest.getUserName());


         } catch (Exception ex) {
             log.error("Exception occurred while retrieving user {} from database , Exception message {}",  authRequest.getUserName(), ex.getMessage());
             throw new UserServiceBusinessException("Exception occurred while fetch user from Database " +  authRequest.getUserName());
         }
     }



}
