package tn.sofrecom.servicegestionacces.service.users;
import tn.sofrecom.servicegestionacces.dto.users.UserRequestDTO;
import tn.sofrecom.servicegestionacces.dto.users.UserResponseDTO;

import java.util.Collection;

public interface UserService {
    UserResponseDTO createNewUser(UserRequestDTO userRequestDTO) ;
    UserResponseDTO updateUser(UserRequestDTO userRequestDTO);
    void deleteUser(Long idUser);
    Collection<UserResponseDTO> getUsers();
    UserResponseDTO getUserById(Long idUser);
}
