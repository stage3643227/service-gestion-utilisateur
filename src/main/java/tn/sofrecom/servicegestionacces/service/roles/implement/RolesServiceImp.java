package tn.sofrecom.servicegestionacces.service.roles.implement;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tn.sofrecom.servicegestionacces.dto.roles.RoleRequestDTO;
import tn.sofrecom.servicegestionacces.dto.roles.RoleResponseDTO;
import tn.sofrecom.servicegestionacces.handlers.permissions.PermissionsNotFoundException;
import tn.sofrecom.servicegestionacces.handlers.permissions.PermissionsServiceBusinessException;
import tn.sofrecom.servicegestionacces.handlers.roles.RolesNotFoundException;
import tn.sofrecom.servicegestionacces.handlers.roles.RolesServiceBusinessException;
import tn.sofrecom.servicegestionacces.mappers.RoleMapper;
import tn.sofrecom.servicegestionacces.model.Permission;
import tn.sofrecom.servicegestionacces.model.Role;
import tn.sofrecom.servicegestionacces.repository.PermissionRepository;
import tn.sofrecom.servicegestionacces.repository.RoleRepository;
import tn.sofrecom.servicegestionacces.service.roles.RolesService;

import java.util.*;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
@Slf4j
@Transactional
public class RolesServiceImp implements RolesService {
    private final RoleRepository roleRepository;
    private final PermissionRepository permissionRepository;

    private final RoleMapper roleMapper;
    /**
     * Creates a new role.
     *
     * @param roleRequestDTO DTO containing the role data
     * @return the DTO containing the newly created role data
     * @throws RolesServiceBusinessException if there is an error creating the role
     */
    @Override
    public RoleResponseDTO createNewRole(RoleRequestDTO roleRequestDTO) {
        RoleResponseDTO roleResponseDTO;
        try {
            log.info("RolesService:createNewRole execution started.");
            Role role = roleMapper.roleDtoToRole(roleRequestDTO);
            List<Long> permissionIds = roleRequestDTO.getPermissionIds();
            if (permissionIds != null && !permissionIds.isEmpty()) {
                Collection<Permission> permissions = new ArrayList<>();
                for (Long permissionId : permissionIds) {
                    Permission permission = permissionRepository.findById(permissionId)
                            .orElseThrow(() -> new PermissionsNotFoundException("Permission not found with id " + permissionId));
                    permissions.add(permission);
                }
                role.setPermissions(permissions);
            }
            log.debug("RolesService:createNewRole request parameters {}", role);
            Role roleResults = roleRepository.saveAndFlush(role);
            log.debug("RolesService:createNewRole received response from Database {}", roleResults);
            roleResponseDTO  = roleMapper.roleToRoleResponseDto(roleResults);
        } catch (Exception ex) {
            log.error("Exception occurred while persisting Role to database , Exception message {}", ex.getMessage());
            throw new RolesServiceBusinessException("Exception occurred while create a new Role");
        }
        log.info("RolesService:createNewRole  execution ended.");
        return roleResponseDTO;
    }
    /**
     * Updates an existing role.
     *
     * @param roleRequestDTO DTO containing the updated role data
     * @return the DTO containing the updated role data
     * @throws RolesServiceBusinessException if there is an error updating the role
     */
    @Override
    public RoleResponseDTO updateRole(RoleRequestDTO roleRequestDTO) {
        RoleResponseDTO roleResponseDTO;
        Role role;
        try {
            role = roleMapper.roleDtoToRole(getRoleById(roleRequestDTO.getIdRole()));
            log.info("RolesService:updateRole execution started.");
            role.setRoleName(roleRequestDTO.getRoleName());
            List<Long> permissionIds = roleRequestDTO.getPermissionIds();
            if (permissionIds != null && !permissionIds.isEmpty()) {
                Collection<Permission> permissions = new ArrayList<>();
                for (Long permissionId : permissionIds) {
                    Permission permission = permissionRepository.findById(permissionId)
                            .orElseThrow(() -> new PermissionsNotFoundException("Permission not found with id " + permissionId));
                    permissions.add(permission);
                }
                role.setPermissions(permissions);
            }
            roleResponseDTO = roleMapper.roleToRoleResponseDto(roleRepository.saveAndFlush(role));
        } catch (Exception ex) {
            log.error("Exception occurred while persisting Company to database , Exception message {}", ex.getMessage());
            throw new RolesServiceBusinessException("Exception occurred while update a Role");
        }
        return roleResponseDTO;
    }
    /**
     * Deletes a role by ID.
     *
     * @param idRole the ID of the role to delete
     * @throws RolesServiceBusinessException if there is an error deleting the role
     */
    @Override
    public void deleteRole(Long idRole) {
        log.debug("Request to delete Role: {}", idRole);
        if (getRoleById(idRole) != null)
            roleRepository.deleteById(idRole);
        else {
            throw new RolesServiceBusinessException("Exception occurred while delete a Role");
        }
    }
    /**
     * Retrieves all roles.
     *
     * @return a collection of {@link RoleResponseDTO} objects
     * @throws PermissionsServiceBusinessException if there is an exception while fetching roles from the database
     */
    @Override
    public Collection<RoleResponseDTO> getRoles() {
        Collection<RoleResponseDTO> roleResponseDTOS;
        try {
            log.info("RolesService:getRoles execution started.");
            Collection<Role> roleList = roleRepository.findAll();

            if (!roleList.isEmpty()) {
                roleResponseDTOS = roleList.stream().sorted(Comparator.comparing(Role::getIdRole))
                        .map(roleMapper::roleToRoleResponseDto)
                        .collect(Collectors.toList());
            } else {
                roleResponseDTOS = Collections.emptyList();
            }
            log.debug("RolesService:getRoles  retrieving Roles from database  {}", roleResponseDTOS);

        } catch (Exception ex) {
            log.error("Exception occurred while retrieving Roles from database , Exception message {}", ex.getMessage());
            throw new RolesServiceBusinessException("Exception occurred while fetch all Roles from Database");
        }

        log.info("PermissionService:getPermissions  execution ended.");
        return roleResponseDTOS;
    }
    /**
     * Retrieves a specific role by its ID.
     *
     * @param idRole the ID of the role to retrieve
     * @return a {@link RoleResponseDTO} object containing the details of the retrieved role
     *  @throws RolesNotFoundException        if the role is not found
     * @throws RolesServiceBusinessException if no role is found with the specified ID
     */
    @Override
    public RoleResponseDTO getRoleById(Long idRole) {
        RoleResponseDTO roleResponseDTO;
        try {
            log.info("RolesService:getRoleById execution started.");
            Role role = roleRepository.findById(idRole)
                    .orElseThrow(() -> new RolesNotFoundException("Role not found with id " + idRole));
            roleResponseDTO = roleMapper.roleToRoleResponseDto(role);
            log.debug("RolesService:getRoleById retrieving role  from database for id {} {}", idRole, roleResponseDTO);

        } catch (Exception ex) {
            log.error("Exception occurred while retrieving role {} from database , Exception message {}", idRole, ex.getMessage());
            throw new RolesServiceBusinessException("Exception occurred while fetch Permission from Database " + idRole);
        }

        log.info("PermissionService:getPermissionById execution ended.");
        return roleResponseDTO;
    }
}
