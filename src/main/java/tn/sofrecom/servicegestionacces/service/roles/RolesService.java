package tn.sofrecom.servicegestionacces.service.roles;
import tn.sofrecom.servicegestionacces.dto.roles.RoleRequestDTO;
import tn.sofrecom.servicegestionacces.dto.roles.RoleResponseDTO;

import java.util.Collection;

public interface RolesService {
    RoleResponseDTO createNewRole(RoleRequestDTO roleRequestDTO) ;
    RoleResponseDTO updateRole(RoleRequestDTO roleRequestDTO);
    void deleteRole(Long idRole);
    Collection<RoleResponseDTO> getRoles();
    RoleResponseDTO getRoleById(Long idRole);
}
