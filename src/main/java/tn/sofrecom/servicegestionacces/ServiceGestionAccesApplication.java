package tn.sofrecom.servicegestionacces;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackages = "tn.*")
public class ServiceGestionAccesApplication {

	public static void main(String[] args) {
		SpringApplication.run(ServiceGestionAccesApplication.class, args);
	}

}
