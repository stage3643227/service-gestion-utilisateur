create table public.permissions
(
    id_permission   bigint not null
        primary key,
    permission_name varchar(255)
);

alter table public.permissions
    owner to postgres;

create table public.roles
(
    id_role   bigint not null
        primary key,
    role_name varchar(255)
);

alter table public.roles
    owner to postgres;
create table public.roles_permissions
(
    id_role       bigint not null
        constraint fkiodo5208nfgl9tgi0evftj9ld
            references public.roles,
    id_permission bigint not null
        constraint fk108af3a237dcjglucidvcnns0
            references public.permissions
);

alter table public.roles_permissions
    owner to postgres;

create table public.users
(
    id_user    bigint not null
        primary key,
    email      varchar(255),
    first_name varchar(255),
    id_company varchar(255),
    last_name  varchar(255),
    password   varchar(255),
    id_role    bigint
        constraint fkt92dgi4412ywy3u8tm9jwdya5
            references public.roles
);

alter table public.users
    owner to postgres;
